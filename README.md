# Equipo Web

## Team members

- Calderon Carbajal Eduardo 
- Hernández Albino Edgar Alejandro
- Macias Gomez Jorge
- Molina Zepeda Natalia Elizeth 
- Rubi Rojas Tania Michelle

## I. Apache
### Instalación
1. Instalamos los paquetes con apt.
```console
$ apt-get install apache2
```
2. Verificamos que la instlación se haya efectuado correctamente.
```console
$ sudo systemctl status apache2.service
● apache2.service - The Apache HTTP Server
     Loaded: loaded (/lib/systemd/system/apache2.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2021-02-24 20:39:39 UTC; 5 days ago
       Docs: https://httpd.apache.org/docs/2.4/
    Process: 115 ExecStart=/usr/sbin/apachectl start (code=exited, status=0/SUCCESS)
    Process: 15247 ExecReload=/usr/sbin/apachectl graceful (code=exited, status=0/SUCCESS)
   Main PID: 128 (apache2)
      Tasks: 6 (limit: 4672)
     Memory: 16.4M
```
3. Verificamos los puertos donde está escuchando.
```console
$ sudo -ntulp | grep apache2
tcp6    0   0 :::80             :::*            LISTEN      18519
/apache2
```
### Configuración
5. Habilitamos el modo SSL.
```console
$ a2enmod ssl
```
6. Ponemos el modo SSL como default.
```console
$ a2ensite default.ssl.
```
7. Reiniciamos el servicio y validamos que ahora también escucha en el puerto 443.
```console
$ systemctl restart apache2
$ sudo -ntulp | grep apache2
tcp6    0   0 :::80             :::*            LISTEN      18519/apache2
tcp6    0   0 :::443            :::*            LISTEN      18519/apache2
```
### Certificado SSL
8. En nuestro caso creamos un certificado wildcard para usarlo en todos los dominios.
```console
$ certboot --authenticatormanual --installer apache -d planbecarios15.cf -d '*.planbecarios15.cf'
```
9. Tendremos que configurar un registro en el servidor DNS de tipo acme-challenge y con el valor que nos muestra: 
```console
Saving debig log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator manual, Installer apache
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Obtaining a new certificate
Performing the following challenges:
dns-01 challenge for redes.tonejito.cf
http-01 challenge for redes.tonejito.cf

-------------------------------------------------------------------------------
NOTE: The IP of this machine will be publicly logged as having requested this
certificate. If you're running certbot in manual mode on a machine that is not
your server, please ensure you're okay with that.

Are you OK with your IP being logged?
-------------------------------------------------------------------------------
(Y)es/(N)o: Y
-------------------------------------------------------------------------------
Please deploy a DNS TXT record under the name
acme-challenge.redes.tonejito.cf with the following value:

Sez6F6xnA13IdmxpaV7q2iM6fnHgr_S1kdmrJTN4a_A

Before continuing, verify the record is deployed.
-------------------------------------------------------------------------------
Press Enter to Continue
```
NOTA: Si alguno de los pasos no se completa se abortará el proceso y se tendrá que iniciar nuevamente. 

10. Una vez terminado el procesos de creación se mostrará un mensaje que nos confirmará que el certificado se ha creado correctamente, además nos dará la hubicación y nombre de los archivos que se generaron:
```console
IMPORTANT NOTES:

- We were unable to set up enhancement redirect for your server, however, we successfully installed your certificate.

Congratulations! Your certificate and chain have been saved at: /etc/letsencrypt/live/planbecarios15.cf/fullchain.pem Your key file has been saved at: /etc/letsencrypt/live/planbecarios15.cf/privkey.pem Your cert will expire on 2022-01-06. To obtain a new or tweaked version of this certificate in the future, simply run certbot again with the "certonly" option. To non-interactively renew *all* of your certificates, run "certbot renew"
```
### Virtualhosts
11. Se tendrá que generar dos archivos de Virtualhost para cada servicio, uno para el dominio y otro para la conexión SSL.
```console
$ /etc/apache2/sites-availables
```
12. El primer archivo wordpress.conf tendrá que tener una estructura como la siguiente: 
```console
<VirtualHost _default_:80>
	#ServerName www.planbecarios15.cf
	#ServerAlias planbecarios15.cf
	#error log
	#accesslog
	
	ErrorLog ${APACHE_LOG_DIR}/default_http_error.log
	CustomLog ${APACHE_LOG_DIR}/default_http_access.log combined
    	
	DocumentRoot /var/www/html
    	<Directory /srv/www/html>
        	Options FollowSymLinks
	        AllowOverride Limit Options FileInfo
	        DirectoryIndex index.php
	        Require all granted
	    </Directory>

RewriteEngine on
RewriteCond %{SERVER_NAME} =www.planbecarios15.cf [OR]
RewriteCond %{SERVER_NAME} =web.planbecarios15.cf [OR]
RewriteCond %{SERVER_NAME} =wordpress.planbecarios15.cf [OR]
RewriteCond %{SERVER_NAME} =planbecarios15.cf
RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>
becarios@web:/var
```
13. El segundo archivo wordpress-ssl.conf tendrá la siguiente estructura: 
```console
<IfModule mod_ssl.c>
<VirtualHost *:443>
	ServerAlias web.planbecarios15.cf
	ServerName planbecarios15.cf
	ServerAlias wordpress.planbecarios15.cf
	ServerAlias www.planbecarios15.cf
	

	ErrorLog ${APACHE_LOG_DIR}/wordpress_error.log
	CustomLog ${APACHE_LOG_DIR}/wordpress_access.log combined

    	DocumentRoot /srv/www/wordpress
    	<Directory /srv/www/wordpress>
        	Options FollowSymLinks
	        AllowOverride Limit Options FileInfo
	        DirectoryIndex index.php
	        Require all granted
	    </Directory>
	<Directory /srv/www/wordpress/wp-content>
	        Options FollowSymLinks
	        Require all granted
	</Directory>

Include /etc/letsencrypt/options-ssl-apache.conf
SSLCertificateFile /etc/letsencrypt/live/planbecarios15.cf/fullchain.pem
SSLCertificateKeyFile /etc/letsencrypt/live/planbecarios15.cf/privkey.pem
</VirtualHost>
</IfModule>
```
IMPORTANTE: cada servicio que se conecte debe tener su propio Virtualhost, para ejemplificar solo se puso el caso de Wordpress.
## Servidores de MariaDB y PostgreSQL
### II. Maria DB
#### Instalación
1. Realizamos la instalación con apt.
```console
$ sudo apt install mariadb-server
```
2. Verificamos la instalación:
```console
$ sudo systemctl  status mariadb
● mariadb.service - MariaDB 10.3.31 database server
   Loaded: loaded (/lib/systemd/system/mariadb.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2021-10-13 20:29:26 CDT; 4 days ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
 Main PID: 625 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 1045)
   Memory: 99.6M
   CGroup: /system.slice/mariadb.service
           └─625 /usr/sbin/mysqld
```
3. Ejecutamos el mysql_secure_installation y configuramos credenciales.
```console
$ sudo mysql_secure_installation
```
4. Para conectarse a MariaDB usamos el siguiente comando:
```console
$ mysql -u root -p
```
### III. PostgreSQL
#### Instalación
1. Realizamos la instalación con apt.
```console
$ sudo apt install postgresql postgresql-contrib
```
2. Iniciamos el servicio con:
```console
$ sudo -u postgres psql
```
3. Verificamos que se este ejecutando.
```console
$ systemctl status postgresql
● postgresql.service - PostgreSQL RDBMS
   Loaded: loaded (/lib/systemd/system/postgresql.service; enabled; vendor preset: enabled)
   Active: active (exited) since Wed 2021-10-13 20:29:28 CDT; 4 days ago
 Main PID: 808 (code=exited, status=0/SUCCESS)
    Tasks: 0 (limit: 1045)
   Memory: 0B
   CGroup: /system.slice/postgresql.service
```
## IV. HP 7.4 FPM
### Instalación desde Sury
## Instalación desde el repositorio Sury
1. Descargamos la clave GPG.
```console
$ sudo apt -y install lsb-release apt-transport-https ca-certificates
```

2. Descargamos y almacenamos el repositorio de PPA en un archivo en nuestro
servidor Debian. 
```console
$ sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org php/apt.gpg
```

3. Agregamos el repositorio.
```console
$ echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
```

4. Instalamos PHP 7.4 en Debian.
```console
$ sudo apt -y install php7.4
```

5. Instalamos la extensión fpm.
```console
$ sudo apt-get install php7.4-fpm
```

Los archivos de configuración PHP FPM se encuentran en el directorio `/etc/php/7.4/fpm/`

6. Verificamos que el servicio php-fpm esté ejecutándose.
```console
$ systemctl status php7.4-fpm
hp7.4-fpm.service - The PHP 7.4 FastCGI Process Manager
   Loaded: loaded (/lib/systemd/system/php7.4-fpm.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2021-10-13 20:29:25 CDT; 4 days ago
     Docs: man:php-fpm7.4(8)
  Process: 574 ExecStartPost=/usr/lib/php/php-fpm-socket-helper install /run/php/php-fpm.sock /etc/php/
 Main PID: 528 (php-fpm7.4)
   Status: "Processes active: 0, idle: 2, Requests: 0, slow: 0, Traffic: 0req/sec"
    Tasks: 3 (limit: 1045)
   Memory: 23.2M
   CGroup: /system.slice/php7.4-fpm.service
           ├─528 php-fpm: master process (/etc/php/7.4/fpm/php-fpm.conf)
           ├─572 php-fpm: pool www
           └─573 php-fpm: pool www
```

### Configuración de handler FastCGI de Apache

## V. WordPress 5.x
### Instalación
1. Creamos un nuevo directorio.
```console
$ sudo mkdir -p /srv/www
```
2. Cambiamos el dueño al nuevo directorio.
```console
$ sudo shown www-data: /srv/www
```
3. Descargamos los archivos en el directorio.
```console
$ curl https://wordpress.org/latest/tar.gz | sudo -u www-data tar zx -C /srv/ww
```
### Configuración
4. Después de crear el Virtualhost del servicio habilitamos nuestro nuevo sitio y habilitamos el mod para realizar rewrites. Deshabilitamos el sitio default. 
```console
$ a2ensite wordpress
$ a2enmod rewrite
$ a2dissite 000-default.conf
$ a2dissite 000-default
$ systemctl reload apache2.service
```
5. Creamos una base de datos en MariaDB para wordpress, creamos un usuario y le damos permisos.
```console
> MariaDB [(none)]> CREATE DATABASE wordpress;
Queery OK, 0 rows affected (0.002 sec)

> MariaDB [(none)]> CREATE USER wordpress@localhost IDENTIFIED BY 'pbsi15WPMariaDB';
Queery OK, 0 rows affected (0.008 sec)

> MariaDB [(none)]> GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, ALTER
    -> ON wordpress.*
    -> TO wordpress@localhost;
Query OK, rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES; Query OK, 9 rows affected (0.002 sec)
MariaDB [(none)]> QUIT
Bye
```
6. Configuramos para conectar wordpress con la BD editando srv/www/wordpress/wp-config.php.
```console
define( 'DB_NAME', 'wordpress' );

define( 'DB_USER', 'wordpress' );

define( 'DB_PASSWORD', 'pbsi15WPMariaDB' );

define('DB_HOST', localhost' );

define( 'DB_CHARSET', 'utf8' );

define( 'DB_COLLATE','' );
```
7. Una vez configurado. Accedemos al login y llenamos los campos: 
    - Título del Sitio: Wordpress Web PBSI.
    - Nombre de usuario: becarios
    - Contraseña: ****************
    - Tu correo alectronico: admin@planbecarios15.cf
    - Visivilidad en los motores de búsqueda: Desactivado

### Conexión a la base de datos MariaDB local

## VI. Redmine 4.x
### Instalación
1. Primero nos convertimos en root y descargamos algunos paquetes.
```console
$ su
$ apt-get install postgresql-8.4 apache2 subversion rake rubygems libopenssl-ruby libpgsql-ruby libapache2-mod-passenger
$ gem install -v=2.3.5 rails
```
2. Antes de configurar redmine necesitamos crear un usuario y una base de datos en Postresql.
```console
$ su postgres
> psql
> postgres=# CREATE ROLE redmine LOGIN ENCRYPTED PASSWORD '<password>' NOINHERIT VALID UNTIL 'infinity';
> postgres=# CREATE DATABASE redmine WITH ENCODING='UTF8' OWNER=redmine TEMPLATE template0;
> postgres=# \q
> exit
```
3. Descargamos Redmine, movemos la carptea y nosa cambiamos a esa ubicación.
```console
$ cd /tmp
$ svn co http://redmine.rubyforge.org/svn/branches/1.0-stable redmine-1.0
$ mv redmine-1.0/ /var/lib/redmine
$ cd /var/lib/redmine
```
### Configuración
4. Primero algunas cosas de la base de datos. Sustituye -password- por tu contraseña.
```console
$ echo "production:
    > adapter: postgresql
    > database: redmine
    > host: localhost
    > username: redmine
    > password: <password>
    > encoding: utf8
    > schema_search_path: public" > config/database.yml

  RAILS_ENV=production rake config/initializers/session_store.rb
  rake generate_session_store
  RAILS_ENV=production rake db:migrate
  RAILS_ENV=production rake redmine:load_default_data 
```
5. Por último, configuramos Apache. Un enlace simbólico al directorio www y la propiedad dada a www-data.
```console
$ ln -s /var/lib/redmine/public /var/www/redmine
$ chown -R www-data:www-data /var/www/redmine
```
6. Habilitamos el servicio y reiniciamos Apache.
```console
$ echo "RailsBaseURI /redmine" > /etc/apache2/sites-available/redmine
$ a2ensite redmine
$ /etc/init.d/apache2 restart
```
### Conexión a la base de datos PostgreSQL

## VII. SquirrelMail
### Instalación
1. Descargamos los paqutes y los descomprimimos.
```console
$ wget https://sourceforge.net/projects/squirrelmail/files/stable/1.4.22/squirrelmail-webmail-1.4.22.zip
$ unzip squirrelmail-webmail-1.4.22.zip
```
2. Movemos la carpeta al directorio /var/www/html y le cambiamos el dueño a www-data.
```console
$ sudo mv squirrelmail-webmail-1.4.22 /var/www/html/
$ sudo chown -R www-data:www-data /var/www/html/squirrelmail-webmail-1.4.22/
```
3. Cambiamos los permisos de la carpeta y la reenombramos.
```console
$ sudo chmod 755 -R /var/www/html/squirrelmail-webmail-1.4.22/
$ sudo mv /var/www/html/squirrelmail-webmail-1.4.22/ /var/www/html/squirrelmail
```
### Configuración
4. Ejecutamos el archivo perl para realizar los cambios y nos dirigimos al apartado Server Settings.
```console
$ sudo perl /var/www/html/squirrelmail/config/conf.pl
```
5. Vamos a cambiar los puertos y el server de IMAP y SMTP, además, cambiaremos el dominio y el server software. Al final veremos los cambios de la siguiente manera:
```console
SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Server Settings

General
-------
1.  Domain                 : planbecarios15.cf
2.  Invert Time            : false
3.  Sendmail or SMTP       : SMTP

IMAP Settings
--------------
4.  IMAP Server            : mail.admin-linux.tonejito.cf
5.  IMAP Port              : array(143,993)
6.  Authentication type    : login
7.  Secure IMAP (TLS)      : false
8.  Server software        : dovecot
9.  Delimiter              : detect

B.  Update SMTP Settings   : mail.admin-linux.tonejito.cf:array(25,587)
H.  Hide IMAP Server Settings

R   Return to Main Menu
C   Turn color on
S   Save data
Q   Quit
```
