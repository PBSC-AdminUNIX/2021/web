#!/bin/bash



cp inputs.txt inputs 2> /dev/null

while [ -s inputs ]; do
    # Leemos la primer linea del archivo. 
    line=$(head -n 1 inputs) 

    username=$(echo $line | cut -d: -f1)
    #encrypted_pass=$(mkpasswd)
    mypwd=$(echo $line | cut -d: -f2)
    userid=$(echo $line | cut -d: -f3)
    groupid=$(echo $line | cut -d: -f4)

    groupname=$(getent group $groupid | cut -d: -f1)

    geckos=$(echo $line | cut -d: -f5)
    newhome=$(echo $line | cut -d: -f6)
    newshell=$(echo $line | cut -d: -f7)


    if [ `sed -n "/^$username/p" /etc/passwd` ]
    then
        >&2 echo "error user exists "
    else

        if [ $(getent group $groupid) ]
        then
            echo "hola"
            
            sudo useradd -u $userid $username

            
            usermod --password $(openssl passwd $mypwd) $username

            #groupmod -g $groupid $username
            usermod -aG $groupname$username

            usermod -c $geckos $username
            mkdir $newhome
            usermod -d $newhome $username
            usermod --shell $newshell $username
            
            
            echo "Created new user."
        else
            >&2 echo "error group doesnt exists "
        fi

    fi

    # Borramos la primer linea
    sed -i '1d' inputs;

done